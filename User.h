#ifndef User_h
#define User_h
#include <string>
using namespace std;

class User{
    private:
        string m_login;
        string m_password;
        string m_name;
        string m_surname;
        string m_bookIsbn;

    public:
        User(const string& login, const string& password, const string& name, const string& surname, const string& bookIsbn);
        User(const string& login, const string& password);
        User(){}; 

        string get_name(void) const {return m_name;}
        string get_surname(void) const {return m_surname;}
        string get_login(void) const {return m_login;}
        string get_password(void) const {return m_password;}
        string get_bookIsbn(void) const {return m_bookIsbn;}

        void set_name(const string& name) {m_name = name;}
        void set_surname(const string& surname) {m_surname = surname;}
        void set_login(const string& login) {m_login = login;}
        void set_password(const string& password) {m_password = password;}
        void set_bookIsbn(const string& bookIsbn) {m_bookIsbn = bookIsbn;}

        void saveUser(const string& txt);
        void deleteUser(const string& login);
        bool searchForUser(const string& login, const string& txt);
        bool userLogin(const string& txt);
        void userMenu();
        void userStatus();
        void userRegistration(const string& txt);
        void searchByISBN(const string& isbn);

        //~User();
};

#endif
