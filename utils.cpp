#include <time.h>
#include <iostream>
#include <limits>
using namespace std;

void wait(const int& seconds)
{   
    clock_t endwait;
    endwait = clock () + seconds * CLOCKS_PER_SEC ;
    while (clock() < endwait) {}
}

void cin_secure(void){
    if (cin.fail())
    {
        cout << "ERROR -- You did not enter an integer\n";
        cin.clear(); 
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        wait(2);
        system("clear");
    }     
}