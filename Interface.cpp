#include <iostream>
#include <fstream>
#include "User.h"
#include "Admin.h"
#include "Book.h"
#include "Interface.h"
using namespace std;

void checkinTime();

void interface(){
    int sel,ex;
    User user;
    Admin admin;
    Book book;
    system("clear");

    

    while(ex!=1){
        cout << "----------------------------LIBRARY MANAGEMENT SYSTEM---------------------------\n";
        cout << endl;
        cout << "[1]-Login as an User [2]-Login as an Admin [3]-Register [any other number]-exit\n";
        cout << endl;
        cout << "Select: ";
        cin >> sel;

        switch (sel){
            case 1:
                if (user.userLogin("users.txt")){
                    checkinTime();
                    user.userMenu();
                }
                break;
            case 2:
                if (admin.userLogin("admins.txt")){
                    checkinTime();
                    admin.adminMenu();    
                }
                break;
            case 3:
                user.userRegistration("users.txt");
                break;
            default:
                ex = 1;
                break;
        }
    }
}


void checkinTime(){
    Book book;
    User user;
    string data,temp;
    fstream file;
    struct tm tm;
    time_t czas;
    time ( &czas );

    file.open("books.txt", ios::in);
    if (file.is_open()){
        while(getline(file,data)){
            temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
            book.set_isbn(temp);
            book.searchForIsbn();
            strptime(book.get_reservation_date().c_str(),"%A%t%B%t%d%t%H:%M:%S%t%Y", &tm);
            time_t t = mktime(&tm);
            if (t<czas && book.get_reservation()=="RESERVED"){
                user.searchByISBN(book.get_isbn());
                user.deleteUser(user.get_login());
                book.resReserve();
                user.set_bookIsbn("");
                user.saveUser("users.txt");
            }
            temp.clear();
        }
    }

}