#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <cstdio>
#include "Book.h"
#include "User.h"
using namespace std;

int main(){
    Book book;
    User user;
    string data,temp;
    fstream file;
    struct tm tm;
    time_t czas;
    time ( &czas );

    file.open("books.txt", ios::in);
    if (file.is_open()){
        while(getline(file,data)){
            temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
            book.set_isbn(temp);
            book.searchForIsbn();
            strptime(book.get_reservation_date().c_str(),"%A%t%B%t%d%t%H:%M:%S%t%Y", &tm);
            time_t t = mktime(&tm);
            if (t<czas){
                user.searchByISBN(book.get_isbn());
                user.deleteUser(user.get_login());
                book.resReserve();
                user.set_bookIsbn("");
                user.saveUser("users.txt");
            }
            temp.clear();
        }
    }

}

