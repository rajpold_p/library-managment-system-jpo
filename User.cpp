#include "User.h"
#include "utils.h"
#include "Book.h"
#include <fstream>
#include <string>
#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

User::User(const string& login= "", const string& password= "", const string& name = "", const string& surname = "", const string& bookIsbn = ""):
m_login{login}, m_password{password}, m_name{name}, m_surname{surname}, m_bookIsbn{bookIsbn}{}

User::User(const string& login= "", const string& password= ""):
m_login{login}, m_password{password} {}

void User::saveUser(const string& txt){
    fstream file;
    file.open(txt, ios::app);
    if(file.is_open()){
        file << m_login << char(33) << m_password << char(34) << m_name << char(35) << m_surname << char(36) << m_bookIsbn << char(37) <<endl;
        file.close();
    }
}

void User::deleteUser(const string& login){

    vector <string> v;
    string data,temp,tp;
    fstream file;
    
    file.open("users.txt", ios::in | ios::out);
    if(file.is_open()){
        while( getline( file, data ) ) {         
            tp.insert(0,data,0,data.find(char(33)));
            if (tp.find(login)==tp.npos || login.size()!=tp.size()){               
                v.push_back(data);
            }
            tp.clear();
        }
        file.close();
    }   

    file.open("users.txt", ios::out | ios::trunc);
    file.close();
        
    file.open("users.txt", ios::out | ios::app);
    if(file.is_open()){
        for(size_t i = 0 ; i < v.size() ; i++){
            file << v[i] << endl;
        }
            
        file.close();
    }
}

bool User::searchForUser(const string& login, const string& txt){
    
    string data, temp, tp;
    fstream file;
    file.open(txt, ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            
            tp.insert(0,data,0,data.find(char(33)));

            if (tp.find(login)!=tp.npos && login.size()==tp.size()){

                temp.insert(0,data,0,data.find(char(33)));
                m_login = temp;
                temp.clear();
            
                temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
                m_password = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
                m_name = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
                m_surname = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);
                m_bookIsbn = temp;
                temp.clear();

                return 1;
            }
            tp.clear();
        }
        file.close();
    }
    return 0;
}

bool User::userLogin(const string& txt){
    
    string login, password;
    int correct;
    
    while (correct!=1){
        cout << "Login: ";
        cin >> login;

        correct = searchForUser(login,txt); 
        
        if (correct==0)
            cout << "There's no such user registered. \n";
    }

    for (int i =2 ; i >=0 ; i--){
        cout << "Password: ";
        cin >> password;

        if (password!=m_password){
            cout << "You typed wrong password! Try again! \n";
            cout << "You have "<< i << " more attempts.\n";
        }
        else{
            cout << "Successfully logged in.\n";
            wait(2);
            system("clear");
            return 1;
        }
    }
    wait(1);
    system("clear");
    return 0;
}

void User::userMenu(){
    string title, author,isbn;
    bool search;
    int sel;
    
    while(sel!=4){
        cout << "----------------------User Menu-------------------------\n";
        cout << endl;
        cout << "[1]-User status  [2]-Reserve book    [3]-List all books\n";
        cout << "[4]-Logout       [any other number]-Return to this menu\n";
        cout << endl;
        cout << "Select: ";
        cin >> sel;
        cin_secure();

        switch (sel){
            case 1:
                userStatus();
                cout << "Press ENTER to continue...\n";
                cin.get();
                cin.get();
                system("clear");
                break;
            case 2:
                {
                    Book book;
                    cout << "Title:";
                    cin >> title;
                    cout << "Author:";
                    cin >> author;
                    cout << "ISBN:";
                    cin >> isbn;
                    search = book.searchForBook(title,author,isbn);
                    if (book.get_reservation()=="NOT AVALIABLE")
                        cout << "Book is not avaliable\n";
                    if (book.get_reservation()=="RESERVED")
                        cout << "Book is already reserved\n";
                    if (m_bookIsbn!="")
                        cout << "You have already reserved or borrowed one book\n";
                    else if( search ){
                        deleteUser(m_login);
                        book.reserve();
                        set_bookIsbn(book.get_isbn());
                        saveUser("users.txt");
                    }
                    else
                        cout << "Such book doesn't exist\n ";
                }
                wait(1);
                break;
            case 3:
                {
                    Book book;
                    book.listAllBooks();
                    break;
                }
            default:
                system("clear");
                break;
        }
    }
}

void User::userStatus(){
    Book book;
    cout << "Name: " << m_name << endl;
    cout << "Surname: " << m_surname << endl;
    cout << "You can issue/reserve only one book\n";
    cout << endl;
    cout << "****************************************************************************************\n";
    book.set_isbn(m_bookIsbn);
    book.searchForIsbn();
    if (m_bookIsbn!="" && book.get_reservation()=="NOT AVALIABLE"){
        cout << "You have already borrowed a book: \n\n";
        cout << "Title: " << book.get_title() << " Author: " << book.get_author() <<  " ISBN: " << book.get_isbn() << endl;
        cout << "You must return it before: " << book.get_reservation_date() << endl << endl;
    }
    
    else if (m_bookIsbn!="" && book.get_reservation()=="RESERVED"){
        string str;
        cout << "You have already reserved a book: \n\n";
        cout << "Title: " << book.get_title() << " Author: " << book.get_author() <<  " ISBN: " << book.get_isbn() << endl << endl;
        while ( str!="y" && str!="n"){
            cout << "Do you want to cancel your reservation? [y/n]: \n";
            cin >> str;
            if (str=="y"){
                deleteUser(m_login);
                set_bookIsbn("");
                book.resReserve();
                saveUser("users.txt");
            }
        }
    }
    else
        cout << "You are free to reserve or borrow any book u want\n";   

    cout << "****************************************************************************************\n";
}

void User::userRegistration(const string& txt)
{

    string login, password1, password2;
    while (1)
    {
        bool correct;
        cout << "Please choose your login: ";
        cin >> login;

        correct = searchForUser(login,txt);

        if (correct == 0)
            break;
        else
            cout << "Login is already used. Please retype login. \n";
    }

    while (1)
    {
        cout << "Please choose your password: ";
        cin >> password1;

        cout << "Retype password: ";
        cin >> password2;

        if (password1 == password2)
            break;
        else
            cout << "Password missmatch!\n";
    }

    string name, surname;
    cout << "Personal information: \n";
    cout << "Type your name: ";
    cin >> name;
    cout << "Type your surname: ";
    cin >> surname;
    cout << endl;

    User temp(login, password1, name, surname);
    temp.saveUser(txt);

    cout << "Registration complete. " << endl;
    wait(2);
    system("clear");
}

void User::searchByISBN(const string& isbn){
    string data, temp, tp;
    fstream file;
    file.open("users.txt", ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            
            tp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);

            if (tp.find(isbn)!=tp.npos && isbn.size()==tp.size()){

                temp.insert(0,data,0,data.find(char(33)));
                m_login = temp;
                temp.clear();
            
                temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
                m_password = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
                m_name = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
                m_surname = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);
                m_bookIsbn = temp;
                temp.clear();
            }
            tp.clear();
        }
        file.close();
    }
}