﻿#Makefile - Library Managment System
CC=g++
CFLAGS=-c -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused

all: LMS

LMS: main.o interface.o User.o Book.o Admin.o utils.o 
	$(CC) main.cpp Interface.cpp User.cpp Book.cpp Admin.cpp utils.cpp -o LMS

main.o: main.cpp	
	$(CC) $(CFLAGS) main.cpp

interface.o: Interface.cpp
	$(CC) $(CFLAGS) Interface.cpp

User.o: User.cpp
	$(CC) $(CFLAGS) User.cpp

Admin.o: Admin.cpp
	$(CC) $(CFLAGS) Admin.cpp

wait.o: utils.cpp
	$(CC) $(CFLAGS) utils.cpp

Book.o: Book.cpp
	$(CC) $(CFLAGS) Book.cpp

clean:
	rm -rf *o LMS

	
