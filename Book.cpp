#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include "Book.h"
#include "User.h"
using namespace std;


Book::Book(const string& title = "", const string& author = "", const string& isbn = "", const string& reservation="AVALIABLE", const string& reservation_date=""):
m_title{title}, m_author{author}, m_isbn{isbn}, m_reservation{reservation}, m_reservation_date{reservation_date}{}

void Book::addBook(){

    fstream file;
    file.open("books.txt", ios::app);
    if(file.is_open()){

        file << m_title << char(33) << m_author << char(34) << m_isbn << char(35) << m_reservation << char(36) << m_reservation_date << char(37) <<endl;

        file.close();
    }
}


bool Book::searchForBook(const string& title, const string& author, const string& isbn){

    string data, temp, tp;
    fstream file;
    file.open("books.txt", ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            
            tp.insert(0,data,0,data.find(char(33)));

            if (tp.find(title)!=tp.npos || title.size()!=tp.size()){

                temp.insert(0,data,0,data.find(char(33)));
                m_title = temp;
                temp.clear();
            
                temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
                m_author = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
                m_isbn = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
                m_reservation = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);
                m_reservation_date = temp;
                temp.clear();
            }

            if(m_title==title && m_author==author && m_isbn==isbn)
                return 1;
            
            tp.clear();
        }
        file.close();
    }
    return 0;
}

bool Book::searchForIsbn(){
    string data, temp, tp;
    fstream file;
    file.open("books.txt", ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            
            tp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);

            if (tp.find(m_isbn)!=tp.npos || m_isbn.size()!=tp.size()){

                temp.insert(0,data,0,data.find(char(33)));
                m_title = temp;
                temp.clear();
            
                temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
                m_author = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
                m_isbn = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
                m_reservation = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);
                m_reservation_date = temp;
                temp.clear();
                
                return 1;
            }
            
            tp.clear();
        }
        file.close();
    }

    return 0;  
}

bool Book::searchForAuthor(){
    int i = 0;
    int n = 1;
    string data, temp, tp;
    fstream file;
    cout << "No\tTitle\tAuthor\tISBN\tStatus\t\tReturn/Res Date\n";
    cout << "************************************************\n";
    file.open("books.txt", ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            
            tp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);

            if (tp.find(m_author)!=tp.npos || m_author.size()!=tp.size()){

                i++;

                temp.insert(0,data,0,data.find(char(33)));
                m_title = temp;
                temp.clear();
            
                temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
                m_author = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
                m_isbn = temp;
                temp.clear();
                 
                temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
                m_reservation = temp;
                temp.clear();

                temp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);
                m_reservation_date = temp;
                temp.clear();
            }

            cout << n++ << ".\t"<< m_title << "\t" << m_author << "\t" << m_isbn << "\t" << m_reservation << "\t" <<
            m_reservation_date << endl;
            
            tp.clear();
        }
        file.close();
    }
    if (i>0)
        return 1;
    else
        return 0; 
}


void Book::listAllBooks(){
    int i=1;
    cout << "No\tTitle\tAuthor\tISBN\tStatus\t\tReturn/Res Date\n";
    cout << "************************************************\n";
    string data, temp, tp;
    fstream file;
    file.open("books.txt", ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            temp.insert(0,data,0,data.find(char(33)));
            m_title = temp;
            temp.clear();
            
            temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
            m_author = temp;
            temp.clear();

            temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
            m_isbn = temp;
            temp.clear();

            temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
            m_reservation = temp;
            temp.clear();

            temp.insert(0,data,data.find(char(36))+1,data.find(char(37))-data.find(char(36))-1);
            m_reservation_date = temp;
            temp.clear();

            cout << i++ << ".\t"<< m_title << "\t" << m_author << "\t" << m_isbn << "\t" << m_reservation << "\t" <<
            m_reservation_date << endl;
        }
        file.close();
    }

}


void Book::deleteBook(){

    vector <string> v;
    string data,tp,tp2,tp3;
    fstream file;
    
    file.open("books.txt", ios::in | ios::out);
    if(file.is_open()){
        while( getline( file, data ) ) {

            tp.insert(0,data,0,data.find(char(33)));
            tp2.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
            tp3.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);

            if ( tp.find(m_title)==tp.npos || tp2.find(m_author)==tp2.npos || m_title.size()!=tp.size() || m_author.size()!=tp2.size() || tp3.find(m_isbn)==tp3.npos || m_isbn.size()!=tp3.size() ){               
                v.push_back(data);
            }

            tp.clear();
            tp2.clear();
            tp3.clear();
        }
        file.close();
    }   

    file.open("books.txt", ios::out | ios::trunc);
    file.close();
        
    file.open("books.txt", ios::out | ios::app);
    if(file.is_open()){
        for(size_t i =0 ; i < v.size() ; i++){
            file << v[i] << endl;
        }
            
        file.close();
    }
}

void Book::reserve(){
    deleteBook();
    m_reservation="RESERVED";
    resTime();
    addBook();
}

void Book::resReserve(){
    deleteBook();
    m_reservation="AVALIABLE";
    m_reservation_date="----";
    addBook();
}

void Book::lend(){
    deleteBook();
    m_reservation="NOT AVALIABLE";
    lendTime();
    addBook();
}

void Book::resTime(){
    time_t czas;
    time( &czas );
    czas=czas+3600*24*2;
    m_reservation_date=asctime(localtime(&czas));
    m_reservation_date.pop_back();
}

void Book::lendTime(){
    time_t czas;
    time( &czas );
    czas=czas+3600*24*15;
    m_reservation_date=asctime(localtime(&czas));
    m_reservation_date.pop_back();

}

