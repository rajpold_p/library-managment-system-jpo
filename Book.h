#ifndef Book_h
#define Book_h

#include <string>
using namespace std;

class Book{
    private:
        string m_title;
        string m_author;
        string m_isbn;
        string m_reservation; //if 1 book is reserved if 0 the book can be issued
        string m_reservation_date;
    public:
        Book(const string& title, const string& author, const string& isbn, const string& reservation, const string& reservation_date);
        Book(){}
        
        string get_title(void) const {return m_title;}
        string get_author(void) const {return m_author;}
        string get_isbn(void) const {return m_isbn;}
        string get_reservation(void) const {return m_reservation;}
        string get_reservation_date(void) const {return m_reservation_date;}

        void set_title(const string& title) {m_title = title;}
        void set_author(const string& author) {m_author = author;}
        void set_isbn(const string& isbn) {m_isbn = isbn;}
        void set_reservation(const string& reservation) {m_reservation = reservation;}
        void set_reservation_date(const string& reservation_date) {m_reservation_date = reservation_date;}
        
        void lendTime();
        void resTime();
        
        void addBook();
        void deleteBook();
        
        void reserve();
        void resReserve();
        void lend();
        void resLend();

        bool searchForBook(const string& title, const string& author, const string& isbn);
        bool searchForAuthor();
        bool searchForIsbn();
        void listAllBooks();

};


#endif