#include <iostream>
#include <fstream>
#include <vector>
#include <cstdio>
#include "Admin.h"
#include "utils.h"
#include "Book.h"

using namespace std;

Admin::Admin(const string& login="", const string& password=""): User(login,password) {}

void Admin::listOfUsers(){
    int i = 1;
    string data, temp, tp;
    fstream file;
    User user;
    cout << "No\tLogin\tPassword\tName\tSurname\n";
    cout << "*******************************************\n";
    file.open("users.txt", ios::in );
    if(file.is_open()){
        while(getline(file,data)){
            
            temp.insert(0,data,0,data.find(char(33)));
            user.set_login(temp);
            temp.clear();
            
            temp.insert(0,data,data.find(char(33))+1,data.find(char(34))-data.find(char(33))-1);
            user.set_password(temp);
            temp.clear();

            temp.insert(0,data,data.find(char(34))+1,data.find(char(35))-data.find(char(34))-1);
            user.set_name(temp);
            temp.clear();

            temp.insert(0,data,data.find(char(35))+1,data.find(char(36))-data.find(char(35))-1);
            user.set_surname(temp);
            temp.clear();

            cout << i++ << ".\t" << user.get_login() << "\t" << user.get_password() << "\t" << user.get_name()
            << "\t" << user.get_surname() << endl;
            
        }
        file.close();
    }
}

void Admin::adminMenu(){
    string title, author, isbn, login;
    int sel;

    struct tm tm;
    time_t czas;
    time ( &czas );
    

    while(sel!=10){
        cout << "-----------------------------Admin Menu--------------------------\n";
        cout << "[1]-List of all Users      [2]-Delete user     [3]-Add new admin \n"; 
        cout << "[4]-Add new book to base   [5]-Delete book     [6]-List all books\n";
        cout << "[7]-List books by author   [8]-Lend book       [9]-Return book\n";
        cout << "[10]-Logout\n";
        cout << endl;
        cout << "Select: ";
        cin >> sel;
        cin_secure();
        switch (sel){
            case 1:
                listOfUsers();
                break;
            case 2:
                cout << "Type login of user you want to remove: ";
                cin >> login;
                if(searchForUser(login,"users.txt")){
                    deleteUser(login);
                    cout << "User succesfully removed\n";
                }
                else
                    cout << "Such user does not exist\n";
                wait(1);
                system("clear");
                break;
            case 3:
                userRegistration("admins.txt");
                break;
            case 4:
                {
                    Book book;
                    cout << "Book tilte: ";
                    cin >> title;
                    cout << "Author: ";
                    cin >> author;
                    cout << "ISBN: ";
                    cin >> isbn;
                    if(!book.searchForBook(title,author,isbn)){
                        Book boook(title,author,isbn,"AVALIABLE","----");
                        boook.addBook();
                    }
                    else{
                        cout << "Book already exist.\n";
                        wait(2);
                    }
                }
                system("clear");
                break;
            case 5:
                {
                    Book book;
                    cout << "DELETING BOOK\n";
                    cout << "Book tilte: ";
                    cin >> title;
                    cout << "Author: ";
                    cin >> author;
                    cout << "ISBN: ";
                    cin >> isbn;
                    
                    if( book.searchForBook(title,author,isbn) ){
                        book.deleteBook();
                        cout << "Book succesfully removed\n";
                    }
                    else
                        cout << "Such book does not exist\n";
                }
                wait(1);
                system("clear");
                break;
            case 6:
                {
                    Book book;
                    book.listAllBooks();
                }
                break;
            case 7:
                {
                    Book book;
                    cout << "Author: ";
                    cin >> author;
                    book.set_author(author);
                    book.searchForAuthor();
                }
                break;
            case 8:
                {
                    Book book;
                    User user;
                    cout << "Type user login: ";
                    cin >> login;
                    cout << "Type book ISBN: ";
                    cin >> isbn;
                    book.set_isbn(isbn);
                    if (user.searchForUser(login,"users.txt") && book.searchForIsbn()){
                        if (user.get_bookIsbn()!=""){
                            cout << "User already reserved or issued a book\n";
                            break;
                        }
                        if (book.get_reservation()!="AVALIABLE"){
                            cout << "Book is already reserved by another user\n";
                            break;
                        }
                        user.deleteUser(user.get_login());
                        book.lend();
                        user.set_bookIsbn(isbn);
                        user.saveUser("users.txt");
                    }
                    else 
                        cout << "User or book doesn't exist\n";
                }
                break;
            case 9:
                {
                    Book book;
                    User user;
                    int payment;
                    cout << "Type user login: ";
                    cin >> login;
                    bool search = user.searchForUser(login,"users.txt");
                    book.set_isbn(user.get_bookIsbn());
                    if ( search && book.searchForIsbn()){
                        if (user.get_bookIsbn()==""){
                            cout << "User didn't reserved or issued any books\n";
                            break;
                        }
                        if (book.get_reservation()=="RESERVED"){
                            cout << "Book is already reserved by this user. Please don't set the book status to AVALIABLE.\n";
                            cout << "If user wants to cancel the reservation he must go to the user panel and press [1]-User status\n";
                            break;
                        }
                        if (book.get_reservation()=="NOT AVALIABLE"){
                            strptime(book.get_reservation_date().c_str(),"%A%t%B%t%d%t%H:%M:%S%t%Y", &tm);
                            time_t t = mktime(&tm);
                            if (t < czas){
                                float fine_days = float(czas-t)/86400;
                                cout << "User overdued this book by " << fine_days << " days\n";
                                cout << "The fine is: " << 5*fine_days << endl;
                                cout << "If user payed his debt press 1 if not press any ohter button: ";
                                cin >> payment;
                                cin_secure();
                                if (payment==1){
                                    user.deleteUser(user.get_login());
                                    book.resReserve();
                                    user.set_bookIsbn(isbn);
                                    user.saveUser("users.txt");
                                    cout << "You chose that user payed his debt. Press enter to proceed." << endl;
                                    cin.get();
                                    cin.get();
                                    system("clear");
                                }
                            }
                            else{
                                user.deleteUser(user.get_login());
                                book.resReserve();
                                user.set_bookIsbn(isbn);
                                user.saveUser("users.txt");
                            }
                           
                        }
                    }
                    else
                        cout << "User or book doesn't exist\n";
                    break;
                }
            default:
                system("clear");
                break;
        }
    }
}
