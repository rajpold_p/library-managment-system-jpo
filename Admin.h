#ifndef Admin_h
#define Admin_h
#include <string>
#include "User.h"
using namespace std;

class Admin:public User{
    private:
        string m_login;
        string m_password;
    
    public:
        Admin(const string& login, const string& password);
        Admin(){};

        void listOfUsers();
        void adminMenu();
};


#endif